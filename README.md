## Code de la route vélo

### Set up
run `npm install` to install the project dependencies.
run `ionic serve` to run the project locally.

### Build Web
ionic build --prod


### Run android
ionic cordova run android

### Run ios
ionic cordova run ios


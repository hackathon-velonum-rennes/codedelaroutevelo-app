import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { take } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CoursService {
  constructor(
    public afs: AngularFirestore
  ) { }


  getAllByCategorie(categorie_id) {
    console.log("getAll cours");
    return new Promise<any>((resolve, reject) => {
      let snapshotChangesRef = this.afs.collection('cours', ref => ref.where('categorie_id', '==', categorie_id));
      snapshotChangesRef.snapshotChanges()
        .pipe(take(1))
        .subscribe(snapshots => {
          let result = snapshots.map(a => {
            const data: any = a.payload.doc.data();
            const id = a.payload.doc["id"];
            return { id, ...data };
          });
          resolve(result);
        }, err => {
          reject(err)
        })
    })
  }

  getById(id) {
    return new Promise<any>((resolve, reject) => {
      this.afs.doc<any>('cours/' + id).valueChanges()
        .pipe(take(1))
        .subscribe(data => {
          resolve({ id, ...data });
        }, err => {
          reject(err)
        })
    });
  }
  getSlidesById(cours_id, id) {
    return new Promise<any>((resolve, reject) => {
      this.afs.doc<any>('cours/' + cours_id + '/slides/' + id).valueChanges()
        .pipe(take(1))
        .subscribe(data => {
          resolve({ id, ...data });
        }, err => {
          reject(err)
        })
    });
  }


  getAllSlidesById(cours_id) {
    console.log("getAll cours");
    return new Promise<any>((resolve, reject) => {
      let snapshotChangesRef = this.afs.collection('cours').doc(cours_id).collection('slides');
      snapshotChangesRef.snapshotChanges()
        .pipe(take(1))
        .subscribe(snapshots => {
          let result = snapshots.map(a => {
            const data: any = a.payload.doc.data();
            const id = a.payload.doc["id"];
            return { id, ...data };
          });
          resolve(result);
        }, err => {
          reject(err)
        })
    })
  }
}

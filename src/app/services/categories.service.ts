import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { take } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  categories = [];
  constructor(
    public afs: AngularFirestore
  ) { }



  getAll() {
    console.log("getAll categories");
    return new Promise<any>((resolve, reject) => {
      let snapshotChangesRef = this.afs.collection('categories');
      snapshotChangesRef.snapshotChanges()
        .pipe(take(1))
        .subscribe(snapshots => {
          let result = snapshots.map(a => {
            const data: any = a.payload.doc.data();
            const id = a.payload.doc["id"];
            return { id, ...data };
          });
          this.categories = result;
          resolve(result);
        }, err => {
          reject(err)
        })
    })
  }

  getById(id) {
    return new Promise<any>((resolve, reject) => {
      this.afs.doc<any>('categories/' + id).valueChanges()
        .pipe(take(1))
        .subscribe(data => {
          resolve({ id, ...data });
        }, err => {
          reject(err)
        })
    });
  }
}

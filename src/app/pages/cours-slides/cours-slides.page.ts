import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoriesService } from 'src/app/services/categories.service';
import { CoursService } from 'src/app/services/cours.service';
import { IonSlides, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-cours-slides',
  templateUrl: './cours-slides.page.html',
  styleUrls: ['./cours-slides.page.scss']
})
export class CoursSlidesPage implements OnInit {
  slides: any;
  cour: any;
  currentIndex = 0;

  @ViewChild('slider', { static: true }) slider: IonSlides;

  constructor(
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private coursService: CoursService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.refresh();
  }

  async slideChanged() {
    // this.currentIndex = this.slides.getActiveIndex();
  }

  close(param = null) {
    this.modalController.dismiss(param);
  }


  async slidePrev() {
    await this.slides.slidePrev();
  }
  async slideNext() {
    await this.slides.slideNext();
  }

  async refresh() {
    this.slides = await this.coursService.getAllSlidesById(this.cour.id);
    console.log('this.cour', this.cour);
    console.log('this.slides', this.slides);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouterModule, Routes } from '@angular/router';

import { QuizzPage } from './quizz.page';
import { QuizzResolver } from './quizz.resolver';
const routes: Routes = [
  {
    path: '',
    component: QuizzPage,
    resolve: {
      data: QuizzResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [QuizzPage],
  providers: [QuizzResolver]
})
export class QuizzPageModule { }

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoriesService } from 'src/app/services/categories.service';
import { CoursService } from 'src/app/services/cours.service';
import { ModalController } from '@ionic/angular';
import { CoursSlidesPage } from '../cours-slides/cours-slides.page';

@Component({
  selector: 'app-quizz',
  templateUrl: './quizz.page.html',
  styleUrls: ['./quizz.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QuizzPage implements OnInit {
  started = false;
  quizz_index = null;
  selected = null;
  show_result = false;
  success = null;
  quizz = [
    {
      categorie: "Panneaux",
      question: 'Que signifie ce panneau ?',
      reponses: [
        "Voie obligatoire pour les cylistes (sans side-car ou remorque)",
        "Voie conseillée pour les cyclistes (sans side-car ou remorque)",
        "Fin de voie réservée à la circulation des véhicules non motorisés et des piétons."
      ],
      reponse: 2,
      picture: 'https://firebasestorage.googleapis.com/v0/b/codedelaroutevelo.appspot.com/o/quizz1.png?alt=media&token=3b5408a8-f6ef-4d51-8ef5-e4de81165be7',
      reponse_text: "La voie verte touche une grande variété de personnes et d'utilisations. Elle se caractérise avant tout par son degré de sécurité et son accessibilité.Elle possède une diagonale rouge lors de la fin voie, et n'en possède pas au début de la voie"
    }
  ]
  constructor(
    private route: ActivatedRoute,
    private categoriesService: CategoriesService,
    private coursService: CoursService,
    private modalController: ModalController
  ) { }

  ngOnInit() {

  }
  select(index) {
    this.selected = index;
  }
  start() {
    this.started = true;
    this.quizz_index = 0;
  }

  result() {
    console.log("result", this.quizz[this.quizz_index], this.selected)
    if (this.quizz[this.quizz_index].reponse == this.selected)
      this.success = true;
    else
      this.success = false;

    this.show_result = true;
  }
  next() {
    this.quizz_index++;
  }

  close() {
    this.modalController.dismiss();
  }
}

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, ActivatedRoute } from "@angular/router";

@Injectable()
export class QuizzResolver implements Resolve<any> {

  constructor() { }

  resolve(route: ActivatedRouteSnapshot) {
    return new Promise((resolve, reject) => {
      console.log("route.paramMap", route.paramMap);
      resolve(route.paramMap.get('id'));
    })
  }
}

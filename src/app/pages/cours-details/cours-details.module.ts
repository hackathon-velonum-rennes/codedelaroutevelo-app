import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RouterModule, Routes } from '@angular/router';

import { CoursDetailsPage } from './cours-details.page';
import { CoursDetailsResolver } from './cours-details.resolver';
import { CoursSlidesPageModule } from '../cours-slides/cours-slides.module';
import { CoursSlidesPage } from '../cours-slides/cours-slides.page';

const routes: Routes = [
  {
    path: '',
    component: CoursDetailsPage,
    resolve: {
      data: CoursDetailsResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CoursSlidesPageModule,

  ],
  entryComponents: [CoursSlidesPage],
  declarations: [CoursDetailsPage],
  providers: [CoursDetailsResolver]
})
export class CoursDetailsPageModule { }

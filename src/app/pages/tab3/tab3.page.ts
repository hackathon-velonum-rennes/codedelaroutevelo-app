import { Component, ViewChild } from '@angular/core';

import { Chart } from 'chart.js';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  @ViewChild('lineChart', { static: true }) lineChart;
  @ViewChild('hrzLineChart', { static: true }) hrzLineChart;
  @ViewChild('hrzLineChart2', { static: true }) hrzLineChart2;
  @ViewChild('hrzLineChart3', { static: true }) hrzLineChart3;

  lines: any;
  hrzLines: any;
  hrzLines2: any;
  hrzLines3: any;
  colorArray: any;

  examens = [
    {
      date: "10/02/2020",
      score: 39
    },
    {
      date: "09/02/2020",
      score: 37
    },
    {
      date: "09/02/2020",
      score: 33
    },
    {
      date: "09/02/2020",
      score: 36
    },
    {
      date: "09/02/2020",
      score: 31
    },
    {
      date: "09/02/2020",
      score: 28
    },
    {
      date: "09/02/2020",
      score: 32
    },
    {
      date: "09/02/2020",
      score: 25
    }
  ]

  constructor() { }


  ionViewDidEnter() {
    this.createAreaChart();
    this.createSimpleLineChart()
    this.createGroupLineChart()
    this.createHrzLineChart3()
  }

  createAreaChart() {
    this.lines = new Chart(this.lineChart.nativeElement, {
      type: 'line',
      data: {
        labels: ['1', '2', '3', '4', '5', '6', '7', '8'],
        datasets: [{
          label: 'Viewers in millions',
          data: [25, 32, 28, 31, 36, 33, 37, 39],
          borderColor: '#8058ff',
          borderWidth: 3,
          fill: false,
        }]
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            display: false,
          }],
          yAxes: [{
            display: true
          }]
        }
      }
    });
  }

  createSimpleLineChart() {
    this.hrzLines = new Chart(this.hrzLineChart.nativeElement, {
      type: 'line',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Viewers in millions',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: 'rgba(0, 0, 0, 0)',
          borderColor: 'rgb(38, 194, 129)',
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  createGroupLineChart() {
    this.hrzLines2 = new Chart(this.hrzLineChart2.nativeElement, {
      type: 'line',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Online viewers in millions',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: 'rgba(0, 0, 0, 0)',
          borderColor: 'rgb(38, 194, 129)',
          borderWidth: 1
        },
        {
          label: 'Offline viewers in millions',
          data: [1.5, 2.8, 3, 4.9, 4.9, 5.5, 7, 12],
          backgroundColor: 'rgba(0, 0, 0, 0)',
          borderColor: 'rgb(242, 38, 19)',
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  createHrzLineChart3() {
    let ctx = this.hrzLineChart3.nativeElement
    ctx.height = 400;
    this.hrzLines3 = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['S1', 'S2', 'S3', 'S4', 'S5', 'S6', 'S7', 'S8'],
        datasets: [{
          label: 'Online viewers in millions',
          data: [2.5, 3.8, 5, 6.9, 6.9, 7.5, 10, 17],
          backgroundColor: 'rgb(242, 38, 19)',
          borderColor: 'rgb(242, 38, 19)',
          borderWidth: 1
        },
        {
          label: 'Offline viewers in millions',
          data: [1.5, 2.8, 3, 4.9, 4.9, 5.5, 7, 12],
          backgroundColor: 'rgb(38, 194, 129)',
          borderColor: 'rgb(38, 194, 129)',
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            },
            stacked: true
          }]
        }
      }
    });
  }
}
